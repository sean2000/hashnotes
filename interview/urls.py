from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    
    url(r'^', 
        include('hashnote.urls', 
            namespace='hashnote', 
            app_name='hashnote')),

)
