import re

from django.db import models
from django.core.urlresolvers import reverse
from django.dispatch import receiver
from django.db.models.signals import pre_save

from tagging.fields import TagField

class Note(models.Model):
    """
    """

    text            = models.TextField()
    date_created    = models.DateTimeField(auto_now_add=True)
    tags            = TagField()
    shortlist       = models.BooleanField()

    class Meta:
        ordering = ['date_created']

    def get_absolute_url(self):
        return reverse('hashnote:home')

    def to_dict(self):
        return {
                'id': self.id,
                'text': self.text,
                'tags': self.tags.split(),
                'date_created': self.date_created.isoformat()
                }

@receiver(pre_save, sender=Note)
def note_tag_handler(sender, instance, **kwargs):

    tags = re.findall('#(\w+)', instance.text)
    instance.tags = ' '.join(tags)

