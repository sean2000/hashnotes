from django import forms

from hashnote.models import Note

class NoteForm(forms.ModelForm):
    """
    """

    text = forms.CharField(
            widget=forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'New Note'
                    }), 
            label='New Note')

    class Meta:
        model = Note
        exclude = ['tags']
