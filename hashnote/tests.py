from django.test import TestCase
from django.core.urlresolvers import reverse

from tagging.models import Tag

from hashnote.models import Note

class NoteTest(TestCase):

    def test_tags(self):
        """
        Confirm creation of tags when new Note added
        """

        note = Note.objects.create(text='This is a test #test1 #test2')

        self.assertEqual(note.tags, 'test1 test2')
        self.assertEqual(Tag.objects.count(), 2)

    def test_views(self):
        """
        Basic view tests
        """

        resp = self.client.get(reverse('hashnote:home'))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get(reverse('hashnote:notes-api'))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(
                reverse('hashnote:notes-api'),
                {'text': 'This is a new note'})

        self.assertEqual(Note.objects.filter(text='This is a new note').count(), 1)

