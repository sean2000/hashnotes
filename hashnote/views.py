import json

from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt

from hashnote.models import Note
from hashnote.forms import NoteForm

@csrf_exempt
def notes_api(request):
    """
    """

    data = []

    if request.method == 'GET':

        notes = Note.objects.all()

        for note in notes:
            data.append(note.to_dict())

    elif request.method == 'POST':
    
        shortlist = request.POST.get('shortlist') == 'on'

        note = Note.objects.create(
                text=request.POST.get('text'),
                shortlist=shortlist
                )

        data.append(note.to_dict())
    
    return HttpResponse(
            json.dumps(data), 
            mimetype='application/json')


def note_list(request, tag=None):
    """
    """

    notes = Note.objects.all()
    shortlist_notes = notes.filter(shortlist=True)

    form = NoteForm()

    if tag:
        notes = notes.filter(tags__contains=tag)

    return render_to_response('hashnote/note_list.html', locals())


