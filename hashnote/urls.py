from django.conf.urls import patterns, include, url

#from hashnote.views import NoteCreateView

urlpatterns = patterns('hashnote.views',

    url(r'^/?$', 
        'note_list',
        name='home'),

    #url(r'^tag/(?P<tag>[0-9A-Za-z-]+)/?$', 
    #    'note_list',
    #    name='tag'),

    #url(r'^add/?$', 
    #    NoteCreateView.as_view(), 
    #    name='create'),

    url(r'^note/?$', 
        'notes_api',
        name='notes-api'),
)
